# Preparing your submission
For preparing submissions, authors are expected to use the LaTeX template provided by ing.grid.

Structure of this template:


| Sl No | Filename | Description |
| ------ | ------ | ------ |
| 1 | acknowledgements.tex | acknowledgements, funding information |
| 2 | authors.tex | authors and affiliations |
| 3 | figures (contains image-a.pdf) | images and pictures (example image) |
| 4 | fonts | needed fonts |
| 5 | inggrid.cls | the documentclass |
| 6 | main.tex | the content |
| 7 | references.bib | the bibliography |


## Compiling the Document
For the preparation of the LaTeX document, authors are welcome to use their preferred LaTeX-compatible writing environment. An up to date `TeX Live` or `MiKTeX` installation, including `biblatex` and `LuaLaTeX` should suffice. 

Alternatively, you can use Overleaf, a user-friendly, web-based, collaborative editor for LaTeX documents. In Overleaf `Menu`, `LuaLaTeX` must be selected as the `Compiler`. 

Please note that when making your submission to the ing.grid preprint server, only the PDF should be uploaded. In case your submission is accepted after the peer review process, you will be asked to submit all files in the LaTeX project as a ZIP archive.

## References
References need to be included in the text using the LaTeX `\cite{}` mechanism as in-text references. Complete bibliographical metadata (including e.g. first names of all authors and editors as well as publication location and publisher for monographs and conference proceedings) need to be provided using the BibTeX file in the LaTeX template. Please make sure the BibTeX file is clean and complete. References and bibliography will be styled automatically.

## Footnotes
Footnotes can be used for extended and/or commented bibliographical references or additional information (but not for regular, single references or lists of references of up to 3 references). They need to be included in the text using the LaTeX `\footnote{}` mechanism. Footnotes will be layouted automatically.

## Figures
Figures are included in the ZIP archive when submitting the article to the ing.grid system. They need to be provided in a vector format (SVG) or in a high resolution pixel-based format (300dpi or better, as JPEG or PNG).

Figures should not be numbered manually, but using the relevant LaTeX mechanism. They need to each have a caption (using `\caption{}`) placed below the figure.

When submitting figures, please note that all copyright issues need to be resolved by the authors. The journal aims to publish all content, including figures, with a Creative Commons Attribution licence. A brief licence statement (name of creator and CC licence) needs to be included in the captions. Please contact the editors if you plan to use figures from third parties that require a different licence.

## SciKGTeX
This repository also allows for the use of [SciKGTex](https://ctan.org/pkg/scikgtex) (Scientific Knowledge Graph TeX), a package that makes it possible to annotate specific research contributions directly in the LaTeX source code. To use this package, uncomment the line `\useScikgtex`, which can be found right above the Date section in `main.tex`.
### Basic Usage
To create a basic contribution, we have to annotate text sections with all of the following 5 standard properties, using the corresponding LaTeX commands:

* _research problem_: `\researchproblem{..}`
* _background_: `\background{..}`
* _method_: `\method{..}`
* _result_: `\result{..}`
* _conclusion_: `\conclusion{..}`

The `main.tex` file of the template contains illustrated examples for each of the 5 properties as comments. They can be found in the _Abstract_ and _Conclusion_ sections. In order to use them, they need to be uncommented. The Latex compiler will show warning messages if any of the 5 properties is not assigned after uncommenting `\useScikgtex`.

### SciKGTeX License
SciKGTeX is published under MIT License.
Copyright (C) 2022 by Christof Bless

## SVG images
If authors prefer to use SVG images, they should uncomment line number 53 in inggrid.cls file, which will facilitate the usage of SVG images. An example for adding an SVG image can be found by uncommenting lines 114-199 of the file main.tex after adding an SVG image to the figures directory. This solution works well in Overleaf/Sharelatex. However, since the svg package requires an Inkscape installation, additional steps are necessary to make the svg package work in Texlive and other local distributions. Interested readers can refer to [https://ctan.org/tex-archive/info/svg-inkscape?lang=en]()
